@extends('layouts.app')
@section('content')
<div class="panel-body">
    <form action="/get" method="GET" class="form-horizontal">
        <div class="form-group">
            <label for="film" class="col-sm-3 control-label">film</label>
            <div class="col-sm-6">
                <input type="text" name="name" id="film-name" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> find film
            </button>
        </div>
    </form>
    <hr>
    <div class="container-fluid">
        @if (count($films) > 0)
            @foreach ($films->all() as $film)
                <div class="row">
                    <div class="col-md-4">{{ $film['name'] }}</div>
                    <div class="col-md-4">{{ $film['begin'] }}</div>
                </div>
                <br>
            @endforeach
            </div>
        @endif
    </div>
</div>
@endsection