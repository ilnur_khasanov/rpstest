<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use App\Film;

class FilmController extends Controller
{
    public function update(Request $request)
    {
        $html = file_get_contents('http://cityopen.ru/?page_id=8831');
        $crawler = new Crawler($html);
        $table = $crawler->filter('table')->eq(1);
        $trs[0] = $table->filter('tr')->eq(2);
        $trs[1] = $table->filter('tr')->eq(3);
        $trs[2] = $table->filter('tr')->eq(4);
        $trs[3] = $table->filter('tr')->eq(5);
        $trs[4] = $table->filter('tr')->eq(6);
        $trs[5] = $table->filter('tr')->eq(7);
        
        if (!empty($trs)) {
            foreach ($trs as $tr) {
                $film = new Film;
                $film->name = $tr->filter('td')->eq(1)->filter('a')->text();
                $film->begin = date("Y-m-d H:i:s", strtotime($tr->filter('td')->eq(0)->text()));
                $film->save();
            }
        }
    }
    
    public function list(Request $request)
    {
        if ($request->has('name')) {
            $searchPhrase = $request->input('name');
            $films = Film::where('name', 'LIKE', '%' . $searchPhrase . '%')->get();
        } else {
            $films = Film::all();
        }
        
        return view('films.list', [
            'films' => $films
        ]);
    }
}
